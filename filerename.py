#!/usr/bin/env python3
from os.path import splitext, isfile, join
from os import listdir, rename
from sys import argv
import hashlib

dirlist = [f for f in listdir(str(argv[1])) if isfile(join(argv[1], f))]

def makehash(tohash):
    blocksize = 65536
    h = hashlib.md5()
    with open(tohash) as f:
        buf = f.read(blocksize)
        while len(buf) > 0:
            h.update(buf)
            buf = f.read(blocksize)
    return h.hexdigest()

for filenames in dirlist:
    name, ext = splitext(filenames)
    s = makehash(filenames)[:10] #TODO have this configurable
    filename = str(s + ext)
    rename(filenames, filename)
